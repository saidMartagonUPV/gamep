package com.spolancom.thread;


// https://www.javaworld.com/article/2074217/java-101--understanding-java-threads--part-1--introducing-threads-and-runnables.html

public class CalculatePi {

    public static void main(String[] args) {
        MyThread mt = new MyThread();
        mt.start();
        try {
            Thread.sleep(1); // Sleep for 1 mlliseconds, 10ms was just enough no my machine for the other threads to finish
        }
        catch (InterruptedException ignoredException) {

        }
        System.out.println("pi = " + mt.pi);
    }
}

class MyThread extends Thread {
    boolean negative = true;

    double pi; // Initialized to 0.0, by default

    public void run() {
        for (int i = 3; i < 100000; i +=2) {
            if (negative) {
                pi -= (1.0 / i);
            }
            else {
                pi += (1.0 / i);
            }
            negative = !negative;
        }
        pi += 1.0;
        pi *= 4.0;
        System.out.println("Finished calculating PI");
    }
}
