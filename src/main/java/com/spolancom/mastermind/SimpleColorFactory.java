package com.spolancom.mastermind;

public class SimpleColorFactory implements ColorFactory {

    @Override
    public Color newColor() {
        return new Color();
    }
}
