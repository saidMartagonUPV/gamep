package com.spolancom.mastermind;

public interface ColorFactory {
    Color newColor();
}
