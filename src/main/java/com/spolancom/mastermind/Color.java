package com.spolancom.mastermind;

/**
 * Represents a color in the Mastermind table.
 */
public class Color {
    /**
     * A special object thats represents a
     * value thst is not a valid color
     */
    public static final Color none = new Color();
}
