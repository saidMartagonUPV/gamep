package com.spolancom.varhandle;

import javax.swing.*;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;

public class AllocArray {

    public static void main(String[] args) {
        VarHandle mh = MethodHandles.arrayElementVarHandle(MyClass.class);
        mh.get();
    }

    public static class MyClass {
        public String name;
        public int age;
    }

}
